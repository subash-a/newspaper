module.exports = function(grunt) {
	grunt.initConfig({
		ts: {
			files: {
				src:["src/**/*.ts"]
			},
			options: {
				tsconfig: "./tsconfig.json"
			}
		},
		tslint: {
			options: {
				configuration: grunt.file.readJSON("tslint.json")
			},
			files: {
				src: ["src/**/*.ts"]
			}
		},
		watch: {
			files: "src/**/*.ts",
			tasks: ["tslint", "ts"]
		}
	});

	grunt.loadNpmTasks("grunt-ts");
	grunt.loadNpmTasks("grunt-tslint");
	grunt.loadNpmTasks("grunt-contrib-watch");
	grunt.registerTask("default",["watch"]);
};
