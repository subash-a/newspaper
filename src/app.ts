///<reference path="../tsd.d.ts" />

interface IMainPage extends IPage {
	title: string;
	date: string;
}

interface IPage {
	pagenumbers: boolean;
	content: Array<IArticle>;
}

interface IArticle {
	header: string;
	content: Array<IParagraph>;
}

interface IParagraph {
	content: string;
}

function main(): void {
	let newspaperProp = HTMLElement.prototype;
	newspaperProp.createdCallback = function() {
		console.log("component created");
	};
	newspaperProp.attributeChangedCallback = function() {
		console.log("attribute updated");
	};
	let newspaper: HTMLElement = document.registerElement("news-paper", {prototype: Object.create(HTMLElement.prototype)});
	let newspage: HTMLElement = document.registerElement("news-page", {prototype: Object.create(HTMLElement.prototype)});
	let newsad: HTMLElement = document.registerElement("news-ad", {prototype: Object.create(HTMLElement.prototype)});

}

// function buildPage(contentObject: Array<IArticle>): void {

// }

// function buildNewspaper(pages: Array<IPage>): void {

// }

main();
